/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#include <chrono>

using namespace std;

struct Timer {
    using Clock = chrono::steady_clock;
    using Time = Clock::time_point;

    void reset() { started = Clock::now(); }
    [[nodiscard]] auto ns() const { return chrono::duration_cast<chrono::nanoseconds>(Clock::now() - started).count(); }
    [[nodiscard]] auto us() const { return chrono::duration_cast<chrono::microseconds>(Clock::now() - started).count(); }
    [[nodiscard]] auto ms() const { return chrono::duration_cast<chrono::milliseconds>(Clock::now() - started).count(); }

    Time started;
};
