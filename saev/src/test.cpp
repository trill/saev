#include <sa/ev.h>
#include <iostream>
#include "timer.h"
#include <limits>

struct DoubleTestCase
{
    std::string_view expr;
    double answer;
};

static constexpr DoubleTestCase DOUBLE_TEST_CASES[] = {
    {"1", 1},
    {"(1)", 1},
    {"-1", -1},
    {"2 + -1", 2 + -1},

    {"pi", sa::ev::builtins::PI},
    {"atan(1)*4 - pi", 0},
    {"e", sa::ev::builtins::E},
    {"min(1 - 4, 3 + 2)", -3},

    {"2+1", 2 + 1},
    {"(((2+(1))))", 2 + 1},
    {"3+2", 3 + 2},

    {"3+2+4", 3 + 2 + 4},
    {"(3+2)+4", 3 + 2 + 4},
    {"3+(2+4)", 3 + 2 + 4},
    {"(3+2+4)", 3 + 2 + 4},

    {"3*2*4", 3 * 2 * 4},
    {"(3*2)*4", 3 * 2 * 4},
    {"3*(2*4)", 3 * 2 * 4},
    {"(3*2*4)", 3 * 2 * 4},

    {"3-2-4", 3 - 2 - 4},
    {"(3-2)-4", (3 - 2) - 4},
    {"3-(2-4)", 3 - (2 - 4)},
    {"(3-2-4)", 3 - 2 - 4},

    {"(3/2)/4", (3.0 / 2.0) / 4.0},
    {"3/(2/4)", 3.0 / (2.0 / 4.0)},
    {"3*(2/4)", 3.0 * (2.0 / 4.0)},

    // What doesn't work is e.g. 1/2/3 because this evaluates from right to left
    {"3/2/4", 3.0 / 2.0 / 4.0},

    {"2*2+1", 2 * 2 + 1},
    {"1+1+1", 1 + 1 + 1},
    {"1 * 2 * 3", 1 * 2 * 3},
    {"1-1-1", 1 - 1 - 1},
    {"1-2-3-4", 1 - 2 - 3 - 4},

    {"(3 * 2 ^ 2)", 3.0 * (2.0 * 2.0)},
    {"(3 * 2 ^ 2 + 4)", 3.0 * (2.0 * 2.0) + 4},
    {"(3 * 2 * 2)", 3.0 * (2.0 * 2.0)},
    {"(3 * 2 * 2 + 4)", 3.0 * (2.0 * 2.0) + 4},
    {"(3 * 2 * -2 + 4)", 3.0 * (2.0 * -2.0) + 4},
    {"2 % 4", 2 % 4},
    // long
    {"(3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) + (3 * 2 * 2 + 4) * 3 + (2 + 4) / "
    "(3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) + (3 * 2 * 2 + 4) * 3 + (2 + 4) / "
    "(3 * 2 * 4) / (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) + (3 * 2 * 2 + 4) * "
    "(3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) + (3 * 2 * 2 + 4) * 3 + (2 + 4) / "
    "3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) + (3 * 2 * 2 + 4) * "
    "3 + (2 + 4) / (3 * 2 * 4)",
    (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) + (3 * 2 * 2 + 4) * 3 + (2 + 4) /
    (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) + (3 * 2 * 2 + 4) * 3 + (2 + 4) /
    (3 * 2 * 4) / (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) + (3 * 2 * 2 + 4) *
    (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) + (3 * 2 * 2 + 4) * 3 + (2 + 4) /
    3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) * (3 * 2 * 2 + 4) * 3 + (2 + 4) / (3 * 2 * 4) + (3 * 2 * 2 + 4) *
    3 + (2 + 4) / (3 * 2 * 4)},

    // Number parsing
    {"5.0095e+11", 5.0095e+11},
    {"-4.44434e+12", -4.44434e+12},
    {"-4.44434e-2", -4.44434e-2},
    {"0x2a", 0x2a},
    {"-0x2a", -0x2a},

};

struct IntTestCase
{
    std::string_view expr;
    int answer;
};

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4554)
#endif
static constexpr IntTestCase INT_TEST_CASES[] = {
    { "1 * 2 << 2 + 4", 1 * 2 << 2 + 4 },
    { "3 & 2", 3 & 2 },
    { "2 | 4", 2 | 4 },
    { "3 ! 2", 3 ^ 2 },
    { "3 ! 2 | 1 * 2 << 2 + 4 >> 2", 3 ^ 2 | 1 * 2 << 2 + 4 >> 2 },
    {"0b01110110", 0b01110110},
};
#ifdef _MSC_VER
#pragma warning(pop)
#endif

template <typename T>
constexpr bool equals(T lhs, T rhs)
{
    return lhs + std::numeric_limits<T>::epsilon() >= rhs &&
        lhs - std::numeric_limits<T>::epsilon() <= rhs;
}

static void run_tests()
{
    for (size_t i = 0; i < sizeof(DOUBLE_TEST_CASES) / sizeof(DoubleTestCase); ++i) {
        const auto& src = DOUBLE_TEST_CASES[i].expr;
        const double answer = DOUBLE_TEST_CASES[i].answer;

        sa::ev::Context<double> ctx;

        sa::ev::Parser<double> parser(ctx);
        Timer timer;
        auto expr = parser(src);
        std::cout << src << std::endl;
        if (expr)
        {
            {
                std::cout << "Interpeted " << std::endl;
                timer.reset();
                const double res = (*expr)(ctx);
                auto stop = timer.ns();
                if (!equals<double>(res, answer))
                {
                    std::cout << "  Failed: " << " expected " << answer << " got " << res << " " << stop << "ns" << std::endl;
                }
                else
                    std::cout << "  Success: " << (*expr)(ctx) << " " << stop << "ns" << std::endl;
                if (!ctx.errors.empty())
                {
                    std::cout << "  Errors:" << std::endl;
                    for (const auto& e : ctx.errors)
                        std::cout << "    " << e.pos << ": " << e.message << std::endl;
                }
            }
            {
                std::cout << "Bytecode " << std::endl;
                sa::ev::bi::Code<double> code;
                expr->Generate(code);
                sa::ev::bi::VM<double> vm(ctx);
                timer.reset();
                const double res = vm.Execute(code);
                auto stop = timer.ns();
                if (!equals<double>(res, answer))
                {
                    std::cout << "  Failed: " << " expected " << answer << " got " << res << " " << stop << "ns" << std::endl;
                }
                else
                    std::cout << "  Success: " << (*expr)(ctx) << " " << stop << "ns" << std::endl;
                if (!ctx.errors.empty())
                {
                    std::cout << "  Errors:" << std::endl;
                    for (const auto& e : ctx.errors)
                        std::cout << "    " << e.pos << ": " << e.message << std::endl;
                }
            }
        }
        else
        {
            std::cout << "  Errors:" << std::endl;
            for (const auto& e : ctx.errors)
                std::cout << "    " << e.pos << ": " << e.message << std::endl;
        }
        std::cout << "===========================================================" << std::endl;
    }
}

static void run_int_tests()
{
    for (size_t i = 0; i < sizeof(INT_TEST_CASES) / sizeof(IntTestCase); ++i) {
        const auto& src = INT_TEST_CASES[i].expr;
        const int answer = INT_TEST_CASES[i].answer;

        sa::ev::Context<int> ctx;

        sa::ev::Parser<int> parser(ctx);
        Timer timer;
        auto expr = parser(src);
        std::cout << src << std::endl;
        if (expr)
        {
            {
                std::cout << "Interpeted " << std::endl;
                timer.reset();
                const int res = (*expr)(ctx);
                auto stop = timer.ns();
                if (!equals<int>(res, answer))
                {
                    std::cout << "  Failed: " << " expected " << answer << " got " << res << " " << stop << "ns" << std::endl;
                }
                else
                    std::cout << "  Success: " << (*expr)(ctx) << " " << stop << "ns" << std::endl;
                if (!ctx.errors.empty())
                {
                    std::cout << "  Errors:" << std::endl;
                    for (const auto& e : ctx.errors)
                        std::cout << "    " << e.pos << ": " << e.message << std::endl;
                }
            }
            {
                std::cout << "Bytecode " << std::endl;
                sa::ev::bi::Code<int> code;
                expr->Generate(code);
                sa::ev::bi::VM<int> vm(ctx);
                timer.reset();
                const double res = vm.Execute(code);
                auto stop = timer.ns();
                if (!equals<double>(res, answer))
                {
                    std::cout << "  Failed: " << " expected " << answer << " got " << res << " " << stop << "ns" << std::endl;
                }
                else
                    std::cout << "  Success: " << (*expr)(ctx) << " " << stop << "ns" << std::endl;
                if (!ctx.errors.empty())
                {
                    std::cout << "  Errors:" << std::endl;
                    for (const auto& e : ctx.errors)
                        std::cout << "    " << e.pos << ": " << e.message << std::endl;
                }
            }
        }
        else
        {
            std::cout << "  Errors:" << std::endl;
            for (const auto& e : ctx.errors)
                std::cout << "    " << e.pos << ": " << e.message << std::endl;
        }
        std::cout << "===========================================================" << std::endl;
    }
}

static void test_eval()
{
    float res = sa::ev::Evaluate<float>("sqrt(pi^1.5+e^2.5)");
    std::cout << "text_eval(): sqrt(pi^1.5+e^2.5) = " << res << std::endl;
    float pi = sa::ev::Evaluate<float>("4*atan(1)");
    std::cout << "text_eval(): pi = " << pi << std::endl;
}
static void test_pow0()
{
    float res = sa::ev::Evaluate<float>("(-1)^0");
    std::cout << "test_pow0(): (-1)^0 = " << res << " should be 1" << std::endl;
}
static void test_rnd()
{
    {
        float res = sa::ev::Evaluate<float>("rnd()");
        std::cout << "test_rnd(): rnd() = " << res << " (should be 0..1)" << std::endl;
    }
    {
        int res = sa::ev::Evaluate<int>("rnd()");
        std::cout << "test_rnd(): rnd() = " << res << " (should be 0..100)" << std::endl;
    }
}

static void test_var()
{
    sa::ev::Context<int> ctx;
    int a = 21;
    // Variables can be modified
    ctx.variables["a"] = &a;
    for (int i = 1; i < 10; ++i)
    {
        a *= i;
        int res = sa::ev::Evaluate<int>("a * 2", ctx);
        std::cout << "a * 2 = " << res << " (should be " << (a * 2) << ")" << std::endl;
    }
}

static void test_vm()
{
    sa::ev::Context<float> ctx;
    float tmp = 0;
    ctx.variables["a"] = &tmp;
    sa::ev::Parser<float> parser(ctx);
    {
        auto expr = parser("(1/(a+1)+2/(a+2)+3/(a+3))");
        sa::ev::bi::Code<float> code;
        expr->Generate(code);
        sa::ev::bi::VM<float> vm(ctx);
        float result = vm.Execute(code);
        std::cout << "(1/(a+1)+2/(a+2)+3/(a+3)) = " << result << " should be " << (*expr)(ctx) << std::endl;

        std::cout << "Dump of expression tree:" << std::endl;
        expr->Dump(std::cout, 0);
        std::cout << "Dump of bytecode:" << std::endl;
        size_t i = 0;
        for (const auto& c : code)
            std::cout << ++i << "\t" << c << std::endl;
    }
    {
        auto expr = parser("min(4, max(7, 8))");
        sa::ev::bi::Code<float> code;
        expr->Generate(code);
        sa::ev::bi::VM<float> vm(ctx);
        float result = vm.Execute(code);
        std::cout << "min(4, max(7, 8)) = " << result << " should be " << (*expr)(ctx) << std::endl;

        std::cout << "Dump of expression tree:" << std::endl;
        expr->Dump(std::cout, 0);
        std::cout << "Dump of bytecode:" << std::endl;
        size_t i = 0;
        for (const auto& c : code)
            std::cout << ++i << "\t" << c << std::endl;
    }
}

static void test_dump()
{
    sa::ev::Context<double> ctx;
    sa::ev::Parser<double> parser(ctx);
    {
        auto expr = parser("sqrt(pi^1.5+e^2.5)");
        std::cout << "Dump sqrt(pi^1.5+e^2.5)" << std::endl;
        expr->Dump(std::cout, 0);
    }
    {
        auto expr = parser("-sqrt(pi^1.5+e^2.5)");
        std::cout << "Dump -sqrt(pi^1.5+e^2.5)" << std::endl;
        expr->Dump(std::cout, 0);
    }
    {
        auto expr = parser("3/4/1");
        std::cout << "Dump 3/4/1" << std::endl;
        expr->Dump(std::cout, 0);
    }
    {
        auto expr = parser("4*atan(1)");
        std::cout << "Dump 4*atan(1)" << std::endl;
        expr->Dump(std::cout, 0);
    }
}

static void test_unary()
{
    {
        sa::ev::Context<double> ctx;
        sa::ev::Parser<double> parser(ctx);
        {
            auto expr = parser("-3");
            std::cout << "-3 = " << (*expr)(ctx) << std::endl;
        }
        {
            auto expr = parser("+3");
            std::cout << "+3 = " << (*expr)(ctx) << std::endl;
        }
        {
            auto expr = parser("-pi");
            std::cout << "-pi = " << (*expr)(ctx) << std::endl;
        }
        {
            auto expr = parser("-sqrt(pi^1.5+e^2.5)");
            std::cout << "-sqrt(pi^1.5+e^2.5) = " << (*expr)(ctx) << std::endl;
        }
    }
    {
        sa::ev::Context<int> ctx;
        sa::ev::Parser<int> parser(ctx);
        {
            auto expr = parser("~3");
            std::cout << "~3 = " << (*expr)(ctx) << std::endl;
        }
    }
}

int main()
{
    test_vm();
    test_var();
    run_tests();
    run_int_tests();
    test_eval();
    test_pow0();
    test_rnd();
    test_unary();
    test_dump();
}
