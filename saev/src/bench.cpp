#include <sa/ev.h>
#include <iostream>
#include "timer.h"
#include <limits>

typedef double (*function1)(double);
#define loops 10000

#define NATIVE
#define INTERPR
#define BYTEC

static void bench(const char* expr, function1 func)
{
    int i, j;
    long long nelapsed = 0;
    long long eelapsed = 0;
    long long belapsed = 0;
    double tmp;
    volatile double d;

    Timer timer;
    sa::ev::Context<double> ctx;
    ctx.variables["a"] = &tmp;
    sa::ev::Parser<double> parser(ctx);
    const auto _expr = parser(expr);

    std::cout << "Expression: " << expr << std::endl;

#ifdef NATIVE
    std::cout << "native ";
    d = 0;
    timer.reset();
    for (j = 0; j < loops; ++j)
        for (i = 0; i < loops; ++i)
        {
            tmp = i;
            d += func(tmp);
        }

    nelapsed = timer.ms();
    std::cout << d;
    std::cout << "\t" << nelapsed << "ms\t" << loops * loops / nelapsed / 1000 << "mfps" << std::endl;
#endif

#ifdef INTERPR
    std::cout << "interp ";
    const auto& e = *_expr;
    d = 0;
    timer.reset();
    for (j = 0; j < loops; ++j)
        for (i = 0; i < loops; ++i)
        {
            tmp = i;
            d += e(ctx);
        }

    eelapsed = timer.ms();
    std::cout << d;
    std::cout << "\t" << eelapsed << "ms\t" << loops * loops / eelapsed / 1000 << "mfps\t" << (((double)eelapsed / nelapsed) - 1.0) * 100.0 << "% longer" << std::endl;
#endif

#ifdef BYTEC
    // Bytecode
    std::cout << "bytec  ";
    sa::ev::bi::Code<double> code;
    _expr->Generate(code);
    sa::ev::bi::VM<double> vm(ctx);
    d = 0;
    timer.reset();
    for (j = 0; j < loops; ++j)
        for (i = 0; i < loops; ++i)
        {
            tmp = i;
            d += vm.Execute(code);
        }

    belapsed = timer.ms();
    std::cout << d;
    std::cout << "\t" << belapsed << "ms\t" << loops * loops / belapsed / 1000 << "mfps\t" << (((double)belapsed / nelapsed) - 1.0) * 100.0 << "% longer" << std::endl;
#endif

    std::cout << std::endl;
}

double a5(double a) {
    return a + 5;
}

double a55(double a) {
    return 5 + a + 5;
}

double a5abs(double a) {
    return fabs(a + 5);
}

double a52(double a) {
    return (a + 5) * 2;
}

double a10(double a) {
    return a + (5 * 2);
}

double as(double a) {
    return sqrt(pow(a, 1.5) + pow(a, 2.5));
}

double al(double a) {
    return (1 / (a + 1) + 2 / (a + 2) + 3 / (a + 3));
}

int main()
{
    bench("a+5", a5);
    bench("5+a+5", a55);
    bench("abs(a+5)", a5abs);

    bench("sqrt(a^1.5+a^2.5)", as);
    bench("a+(5*2)", a10);
    bench("(a+5)*2", a52);
    bench("(1/(a+1)+2/(a+2)+3/(a+3))", al);
}
