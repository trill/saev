/**
 * Copyright (c) 2021, Stefan Ascher
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#include <sa/ev.h>
#include <iostream>
#include "timer.h"
#include <limits>
#include <string>
#include <cstring>
#include <map>
#include <iomanip>
#ifdef USE_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif
#include "version.h"

enum class Type
{
    Int,
    Int64,
    Float,
    Double
};
enum class OutputFormat
{
    Dec,
    Hex,
    Oct,
    Bin
};

static Type type = Type::Float;
static OutputFormat outputFormat = OutputFormat::Dec;
static bool dump = false;
static bool bi = false;
static int precision = 10;

template <typename T, typename U>
[[nodiscard]] constexpr bool is_set(T bit_set, U bits)
{
    return (bit_set & static_cast<T>(bits)) == static_cast<T>(bits) && (bits != 0 || bit_set == static_cast<T>(bits));
}
template <typename charType>
inline std::basic_string<charType> trim(const std::basic_string<charType>& str,
    const std::basic_string<charType>& whitespace = " \t")
{
    // Left
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return std::basic_string<charType>(); // no content

    // Right
    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

inline bool is_White(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isspace(*it)) ++it;
    return !s.empty() && it == s.end();
}

inline std::vector<std::string> split(const std::string& str, const std::string& delim, bool keepEmpty = false, bool keepWhite = true)
{
    std::vector<std::string> result;

    size_t beg = 0;
    for (size_t end = 0; (end = str.find(delim, end)) != std::string::npos; ++end)
    {
        auto substring = str.substr(beg, end - beg);
        beg = end + delim.length();
        if (!keepEmpty && substring.empty())
            continue;
        if (!keepWhite && is_White(substring))
            continue;
        result.push_back(substring);
    }

    auto substring = str.substr(beg);
    if (substring.empty())
    {
        if (keepEmpty)
            result.push_back(substring);
    }
    else if (is_White(substring))
    {
        if (keepWhite)
            result.push_back(substring);
    }
    else
        result.push_back(substring);

    return result;
}


template<typename T>
static bool Eval(const std::string& _expr)
{
    static T lastResult = {};
    static std::map<std::string, T> assignments;
    sa::ev::Context<T> ctx;
    for (const auto& a : assignments)
        ctx.variables[a.first] = &a.second;

    sa::ev::Parser<T> parser(ctx);
    // Make assignment possible, store result as constant
    const auto parts = split(_expr, "=", false, false);
    std::string exprString;
    std::string assignTo;
    if (parts.size() == 1)
    {
        exprString = parts[0];
    }
    else if (parts.size() == 2)
    {
        assignTo = trim<char>(parts[0]);
        if (assignTo.length() == 0 || !isalpha(assignTo[0]) || assignTo.find(" ") != std::string::npos)
        {
            std::cerr << "\"" << assignTo << "\" is not a valid identifier" << std::endl;
            assignTo.clear();
        }
        exprString = parts[1];
    }
    else
    {
        std::cerr << "Multiple assignments in " << _expr << std::endl;
        return false;
    }
    auto expr = parser(exprString);

    if (!expr)
    {
        for (const auto& e : ctx.errors)
            std::cerr << "  " << e << std::endl;
        return false;
    }

    sa::ev::bi::Code<T> code;
    if (bi)
    {
        // Use bytecode interpreter
        expr->Generate(code);
        sa::ev::bi::VM<T> vm(ctx);
        lastResult = vm.Execute(code);
    }
    else
        lastResult = (*expr)(ctx);

    if (outputFormat == OutputFormat::Bin && (type == Type::Int || type == Type::Int64))
    {
        if constexpr (std::is_integral<T>::value)
        {
            constexpr size_t c = sizeof(T) * 8;
            std::string result;
            result.resize(c);
            for (size_t i = 1; i <= c; ++i)
                result[c - i] = is_set(lastResult, 1 << (i - 1)) ? '1' : '0';
            std::cout << result << std::endl;
        }
    }
    else
    {
        if (outputFormat == OutputFormat::Hex)
            std::cout << std::hex;
        else if (outputFormat == OutputFormat::Oct)
            std::cout << std::oct;
        else
            std::cout << std::dec;
        std::cout << std::setprecision(precision);
        std::cout << lastResult << std::endl;
    }
    if (!ctx.errors.empty())
    {
        for (const auto& e : ctx.errors)
            std::cerr << "  " << e << std::endl;
        return false;
    }
    if (dump)
    {
        std::cout << "Dump of expression tree:" << std::endl;
        expr->Dump(std::cout, 0);
        if (bi)
        {
            std::cout << "Dump of bytecode:" << std::endl;
            size_t i = 0;
            for (const auto& c : code)
                std::cout << ++i << "\t" << c << std::endl;
        }
    }
    if (!assignTo.empty())
        assignments[assignTo] = lastResult;
    return true;
}

static bool HandleCommand(const std::string& input)
{
    if (input == "help" || input == "h" || input == "?")
    {
        std::cout << "Commands start with `:` except `quit`. More commands can be separated with a space, e.g. `:int hex`" << std::endl;
        std::cout << "Available commands" << std::endl;
        std::cout << "  q, quit, exit: Quit" << std::endl;
        std::cout << "  prec: Set precision :prec=12" << std::endl;
        std::cout << "  bi: Use bytecode interpreter instead of expression tree interpreter" << std::endl;
        std::cout << "  h, help, ?: Show this help" << std::endl;
        std::cout << "  dump: Dump Expression tree" << std::endl;
        std::cout << "Types" << std::endl;
        std::cout << "  int: All calulations are done with int type" << std::endl;
        std::cout << "  int64: All calulations are done with int64 type" << std::endl;
        std::cout << "  float: All calulations are done with float type" << std::endl;
        std::cout << "  double: All calulations are done with double type" << std::endl;
        std::cout << "Output format" << std::endl;
        std::cout << "  dec: Set output format to decimal" << std::endl;
        std::cout << "  hex: Set output format to hexadecimal" << std::endl;
        std::cout << "  oct: Set output format to octal" << std::endl;
        std::cout << "  bin: Set output format to binary" << std::endl;
        return true;
    }
    if (input == "dump")
    {
        dump = !dump;
        std::cout << "Dump expression tree is " <<  (dump ? "on" : "off") << std::endl;
        return true;
    }
    if (input.starts_with("prec"))
    {
        const auto parts = split(input, "=", false, false);
        if (parts.size() < 2)
        {
            std::cerr << "Missing argument for prec. Format is prec=<value>, e.g. prec=10" << std::endl;
            return false;
        }
        precision = atoi(parts[1].c_str());
        std::cout << "Precision is " <<  precision << std::endl;
        return true;
    }
    if (input == "bi")
    {
        bi = !bi;
        std::cout << "Bytecode interpreter is " <<  (bi ? "on" : "off") << std::endl;
        return true;
    }
    if (input == "int")
    {
        if (type == Type::Int)
            return true;
        type = Type::Int;
        std::cout << "Type is int now" << std::endl;
        return true;
    }
    if (input == "int64")
    {
        if (type == Type::Int64)
            return true;
        type = Type::Int64;
        std::cout << "Type is int64 now" << std::endl;
        return true;
    }
    if (input == "float")
    {
        if (type == Type::Float)
            return true;
        type = Type::Float;
        std::cout << "Type is float now" << std::endl;
        HandleCommand("dec");
        return true;
    }
    if (input == "double")
    {
        if (type == Type::Double)
            return true;
        type = Type::Double;
        std::cout << "Type is double now" << std::endl;
        HandleCommand("dec");
        return true;
    }
    if (input == "dec")
    {
        if (outputFormat == OutputFormat::Dec)
            return true;
        outputFormat = OutputFormat::Dec;
        std::cout << "Output format is now Decimal" << std::endl;
        return true;
    }
    if (input == "hex")
    {
        if (outputFormat == OutputFormat::Hex)
            return true;
        outputFormat = OutputFormat::Hex;
        std::cout << "Output format is now Hexadecimal" << std::endl;
        if (type != Type::Int && type != Type::Int64)
            HandleCommand("int");
        return true;
    }
    if (input == "oct")
    {
        if (outputFormat == OutputFormat::Oct)
            return true;
        outputFormat = OutputFormat::Oct;
        std::cout << "Output format is now Octal" << std::endl;
        if (type != Type::Int && type != Type::Int64)
            HandleCommand("int");
        return true;
    }
    if (input == "bin")
    {
        if (outputFormat == OutputFormat::Bin)
            return true;
        outputFormat = OutputFormat::Bin;
        std::cout << "Output format is now Binary" << std::endl;
        if (type != Type::Int && type != Type::Int64)
            HandleCommand("int");
        return true;
    }
    std::cerr << "Unknown command \"" << input << "\"" << std::endl;
    return false;
}

static std::string GetPropmt()
{
    std::stringstream ss;
    switch (type)
    {
    case Type::Int:
        ss << "int ";
        break;
    case Type::Int64:
        ss << "int64 ";
        break;
    case Type::Float:
        ss << "float ";
        break;
    case Type::Double:
        ss << "double ";
        break;
    default:
        break;
    }
    switch (outputFormat)
    {
    case OutputFormat::Dec:
        ss << "dec";
        break;
    case OutputFormat::Hex:
        ss << "hex";
        break;
    case OutputFormat::Oct:
        ss << "oct";
        break;
    case OutputFormat::Bin:
        ss << "bin";
        break;
    default:
        break;
    }
    ss << "> ";
    return ss.str();
}

static bool GetInput(std::string& input)
{
#ifdef USE_READLINE
    char* i = readline(GetPropmt().c_str());
    if (!i)
        return false;
    input.assign(i);
    free(i);
    return true;
#else
    std::cout << GetPropmt();
    return static_cast<bool>(std::getline(std::cin, input));
#endif
}

static void Repl()
{
#ifdef USE_READLINE
    rl_bind_key('\t', rl_complete);
#endif
    std::cout << "sa::ev simple REPL" << std::endl;
    std::cout << "Copyright (C) 2021 Stefan Ascher" << std::endl;
    std::cout << "Type `:help` for help" << std::endl << std::endl;
    std::string input;
    while (GetInput(input))
    {
        bool success = false;
        std::string linput = trim<char>(input);
        if (linput.empty())
            continue;
        if (linput == "q" || linput == "quit" || linput == "exit")
            break;
        if (linput.starts_with(":"))
        {
            linput.erase(0, 1);
            success = true;
            const auto cmds = split(linput, " ", false, false);
            for (const auto& cmd : cmds)
            {
                if (!HandleCommand(cmd))
                    success = false;
            }
        }
        else
        {
            switch (type)
            {
            case Type::Int:
                success = Eval<int>(linput);
                break;
            case Type::Int64:
                success = Eval<long long>(linput);
                break;
            case Type::Float:
                success = Eval<float>(linput);
                break;
            case Type::Double:
                success = Eval<double>(linput);
                break;
            default:
                break;
            }
        }
#ifdef USE_READLINE
        if (success)
            add_history(input.c_str());
#else
        (void)success;
#endif
    }
}

static void ShowHelp(const char* arg0)
{
    std::cout << "sa::ev simple REPL" << std::endl;
    std::cout << "Usage: " << arg0 << " [-<options>] [<expression>]" << std::endl;
    std::cout << "  i             Calculate with integers" << std::endl;
    std::cout << "  I             Calculate with 64 Bit integers" << std::endl;
    std::cout << "  f             Calculate with floats" << std::endl;
    std::cout << "  d             Calculate with doubles" << std::endl;
    std::cout << "  c             Decimal output format" << std::endl;
    std::cout << "  x             Hexadecimal output format (int only)" << std::endl;
    std::cout << "  o             Octal output format (int only)" << std::endl;
    std::cout << "  b             Binary output format (int only)" << std::endl;
    std::cout << "  P <precision> Output precision" << std::endl;
    std::cout << "  p             Dump expression tree" << std::endl;
    std::cout << "  B             Use bytecode interpreter" << std::endl;
    std::cout << "  h             Show help" << std::endl;
    std::cout << "  v             Show program version" << std::endl;
}

static std::string ReadStdIn()
{
    std::string line;
    std::stringstream ss;
    while (std::getline(std::cin, line))
    {
        ss << line << '\n';
    }
    return ss.str();
}

int main(int argc, char** argv)
{
    if (argc == 1)
    {
        Repl();
        return 0;
    }

    int c;
    while ((c = getopt(argc, argv, "iIfdcxobP:pBhv")) != -1)
    {
        switch (c) {
        case 'i':
            type = Type::Int;
            break;
        case 'I':
            type = Type::Int64;
            break;
        case 'f':
            type = Type::Float;
            outputFormat = OutputFormat::Dec;
            break;
        case 'd':
            type = Type::Double;
            outputFormat = OutputFormat::Dec;
            break;
        case 'c':
            outputFormat = OutputFormat::Dec;
            break;
        case 'x':
            type = Type::Int;
            outputFormat = OutputFormat::Hex;
            break;
        case 'o':
            type = Type::Int;
            outputFormat = OutputFormat::Oct;
            break;
        case 'b':
            type = Type::Int;
            outputFormat = OutputFormat::Bin;
            break;
        case 'P':
            precision = atoi(optarg);
            break;
        case 'p':
            dump = true;
            break;
        case 'B':
            bi = true;
            break;
        case 'h':
            ShowHelp(argv[0]);
            exit(0);
        case 'v':
            std::cout << "sa::ev " << SAEV_VERSION_STRING << std::endl;
            exit(0);
        default:
            break;
        }
    }
    std::string expr;
    if (optind < argc)
        expr = argv[optind++];

    if (expr.empty())
    {
        Repl();
        return 0;
    }
    if (expr == "-")
    {
        expr = ReadStdIn();
        if (expr.empty())
        {
            return 1;
        }
    }

    switch (type)
    {
    case Type::Int:
        return Eval<int>(expr) ? 0 : 1;
    case Type::Int64:
        return Eval<long long>(expr) ? 0 : 1;
    case Type::Float:
        return Eval<float>(expr) ? 0 : 1;
    case Type::Double:
        return Eval<double>(expr) ? 0 : 1;
    default:
        return 1;
    }
}
