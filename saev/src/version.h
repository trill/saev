/**
 * Copyright (c) 2021-2022, Stefan Ascher
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

#define SAEV_VERSION_MAJOR 1
#define SAEV_VERSION_MINOR 4

#define SAEV_VERSION_STR(a) #a
#define SAEV_VERSION_STR1(a) SAEV_VERSION_STR(a)

#define SAEV_VERSION_STRING SAEV_VERSION_STR1(SAEV_VERSION_MAJOR) "." SAEV_VERSION_STR1(SAEV_VERSION_MINOR)
