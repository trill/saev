# sa::ev

Simple single header mathematical expression evaluator with support for constants,
variables and functions with up to four arguments.

## License

GPL 3.0

## Requirements

* C++20 compiler
* CMake to compile the test/demo/REPL
* The REPL compiles only on Linux and BSD because of `getopt()`

## Usage

All you need is the `ev.h` header, so just `#include <sa/ev.h>`.

### Short

~~~cpp
#include <iostream>
#include <sa/ev.h>

int main()
{
    // Using the sa::ev::Evaluate() convenience function

    // Create the context with `int`s. All calculations are done with integers.
    sa::ev::Context<int> ctx;
    ctx.constants["a"] = 21;
    int res = sa::ev::Evaluate<int>("a * 2", ctx);
    std::cout << "a * 2 = " << res << " (should be 42)" << std::endl;
}
~~~

Same as above, but use the default context.

~~~cpp
#include <iostream>
#include <sa/ev.h>

int main()
{
    // Using the sa::ev::Evaluate() convenience function with the default context
    int res = sa::ev::Evaluate<int>("5 * 2");
    std::cout << "5 * 2 = " << res << " (should be 10)" << std::endl;
}
~~~

### Variables

~~~cpp
#include <iostream>
#include <sa/ev.h>

int main()
{
    sa::ev::Context<int> ctx;
    int a = 21;
    // In contrast to constants, variables can be modified. Constants are also expanded
    // at compile time when possible, while variables are always evaluated at runtime.
    ctx.variables["a"] = &a;
    // If there is a variable with the same name as a constant, it always prefers
    // the variable.
    for (int i = 1; i < 10; ++i)
    {
        a *= i;
        int res = sa::ev::Evaluate<int>("a * 2", ctx);
        std::cout << "a * 2 = " << res << " (should be " << (a * 2) << ")" << std::endl;
    }
}
~~~

### Function call

~~~cpp
#include <iostream>
#include <sa/ev.h>

int main()
{
    // Create a context, we are using floats
    // The Context is for adding constants, variables and functions
    sa::ev::Context<float> ctx;
    ctx.constants["a"] = 42.0f;
    // Add the function the_answer_2() which takes one argument
    ctx.functions["the_answer_2"] = [](float v) -> float {
        return v / 2;
    };
    // The source to be evaluated
    constexpr const char* Source = R"src(the_answer_2(a) * 2)src";
    // Create a Parser with the context
    sa::ev::Parser<float> parser(ctx);
    // Parse the expression
    auto expr = parser(Source);
    if (expr)
    {
        // Evaluate the expression
        float val = (*expr)(ctx);
        std::cout << Source << " = " << val << " (should be 42)" << std::endl;
    }
    else
    {
        // If there are errors it returns null
        std::cout << "Errors:" << std::endl;
        for (const auto& e : ctx.errors)
            std::cout << e.pos << ": " << e.message << std::endl;
    }
}
~~~

**Note** Identifiers, such as constants, variables and functions, are case sensitive.

### Compile options

* `POWER_OPERATOR` Compile with power operator (`^`). In this case the Xor operator
will turn from `^` to `!`.

## Details

### Number literals

Numbers can be entered in decimal, hexadecimal, octal and binary format.

* Decimal: `12.7e+2`
* Hexadecimal: `0x2a`
* Octal: `0o16`
* Binary: `0b101010`

### Operators

* `&`, `^`, `|` Bitwise and, xor (when compiled with `POWER_OPERATOR` xor becomes `!`) and or
* `<<`, `>>` Left, right bit shift
* `+`, `-` Sum
* `*`, `/`, `%` Product
* `^` Power, only when compiled with `POWER_OPERATOR`
* `-`, `+`, `~` Unary

### Constants

Built in constants:
* `e` ℇ
* `inf` Infinity
* `pi` π

Custom constants can easily be added:
~~~cpp
ctx.constants["the_answer"] = 42;
~~~

### Functions

Built in functions:
* `abs(T)`
* `acos(T)`
* `asin(T)`
* `atan(T)`
* `atan2(T, T)`
* `ceil(T)`
* `clamp(T value, T min, T max)`
* `cos(T)`
* `cosh(T)`
* `exp(T)`
* `fac(T)`
* `floor(T)`
* `ln(T)`
* `log10(T)`
* `max(T, T)`
* `min(T, T)`
* `pow(T, T)`
* `rnd()`
* `sign(T)`
* `sin(T)`
* `sinh(T)`
* `sqrt(T)`
* `tan(T)`
* `tanh(T)`
* `trunc(T)`

Floating point types only:
* `deg(T)` Rad to Deg
* `fract(T)`
* `lerp(T lhs, T rhs, T i)`
* `rad(T)` Deg to Rad

Custom functions can easily be added:
~~~cpp
sa::ev::Context<int> ctx;
ctx.functions["my_weird_function"] = [](int x, int y) { return x * y; };
int res = sa::ev::Evaluate<int>("my_weird_function(2, 4)", ctx);
// res should be 8
~~~

**Note** Functions can have up to four arguments, if you need more, you must extend
the library.

### Variables

In contrast to constants, variables can be modified. Constants are also expanded
at compile time when possible, while variables are always evaluated at runtime.

~~~cpp
// Adding a variable
sa::ev::Context<int> ctx;
int a = 21;
ctx.variables["a"] = &a;
~~~

## REPL

The program `saev` is a simple REPL and command line evaluator.

Calculation results can be assigned to variables, e.g.:
~~~plain
float dec> a=pi*4
15.5664
float dec> a*a
157.914
~~~

### Usage

~~~sh
saev [-<options>] [<expression>]
~~~

* `i`: Calculate with integers
* `I`: Calculate with 64 Bit integers
* `f`: Calculate with floats
* `d`: Calculate with doubles
* `c`: Decimal output format
* `x`: Hexadecimal output format (int only)
* `o`: Octal output format (int only)
* `b`: Binary output format (int only)
* `P`: Set output precision
* `p`: Dump expression tree
* `B`: Use bytecode interpreter
* `h`: Show help
* `v`: Show program version
